package mlog

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var globalLogger *Logger

func InitGlobalLogger(logger *Logger) {
	glob := *logger
	glob.zap = glob.zap.WithOptions(zap.AddCallerSkip(1))
	globalLogger = &glob
	Debug = globalLogger.Debug
	Info = globalLogger.Info
	Warn = globalLogger.Warn
	Error = globalLogger.Error
	Critical = globalLogger.Critical
	Panic = globalLogger.Panic
	Debugf = globalLogger.Debugf
	Infof = globalLogger.Infof
	Warnf = globalLogger.Warnf
	Errorf = globalLogger.Errorf
}

func RedirectStdLog(logger *Logger) {
	_, _ = zap.RedirectStdLogAt(logger.zap.With(zap.String("source", "stdlog")).WithOptions(zap.AddCallerSkip(-2)), zapcore.ErrorLevel)
}

type LogFunc func(string, ...Field)
type logFuncF func(format string, v ...interface{})

var Debug LogFunc = defaultDebugLog
var Info LogFunc = defaultInfoLog
var Warn LogFunc = defaultWarnLog
var Error LogFunc = defaultErrorLog
var Critical LogFunc = defaultCriticalLog
var Panic LogFunc = defaultPanicLog

var Debugf logFuncF = defaultDebugLogf
var Infof logFuncF = defaultInfoLogf
var Warnf logFuncF = defaultWarnLogf
var Errorf logFuncF = defaultErrorLogf
