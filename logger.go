package mlog

import (
	"fmt"
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

const (
	// Very verbose messages for debugging specific issues
	LevelDebug = "debug"
	// Default log level, informational
	LevelInfo = "info"
	// Warnings are messages about possible issues
	LevelWarn = "warn"
	// Errors are messages about things we know are problems
	LevelError = "error"
)

// Type and function aliases from zap to limit the libraries scope into code
type Field = zapcore.Field

var Int = zap.Int
var Uint32 = zap.Uint32
var Int32 = zap.Int32
var Int64 = zap.Int64
var String = zap.String
var Any = zap.Any
var Err = zap.Error
var NamedErr = zap.NamedError
var Bool = zap.Bool
var Duration = zap.Duration
var StackTrace = zap.Stack

type Configuration struct {
	EnableConsole          bool
	ConsoleJson            bool
	ConsoleLevel           string
	EnableFile             bool
	FileJson               bool
	FileLevel              string
	FileLocation           string
	HumanReadableTimestamp bool
}

type Logger struct {
	zap          *zap.Logger
	consoleLevel zap.AtomicLevel
	fileLevel    zap.AtomicLevel
}

func getZapLevel(level string) zapcore.Level {
	switch level {
	case LevelDebug:
		return zapcore.DebugLevel
	case LevelInfo:
		return zapcore.InfoLevel
	case LevelWarn:
		return zapcore.WarnLevel
	case LevelError:
		return zapcore.ErrorLevel
	default:
		return zapcore.InfoLevel
	}
}

func makeEncoder(json bool, humanReadableTimestamp bool) zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	if humanReadableTimestamp {
		encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	}

	if json {
		return zapcore.NewJSONEncoder(encoderConfig)
	}

	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func NewLogger(config *Configuration) *Logger {
	var cores []zapcore.Core

	logger := &Logger{
		consoleLevel: zap.NewAtomicLevelAt(getZapLevel(config.ConsoleLevel)),
		fileLevel:    zap.NewAtomicLevelAt(getZapLevel(config.FileLevel)),
	}

	if config.EnableConsole {
		writer := zapcore.Lock(os.Stderr)
		core := zapcore.NewCore(makeEncoder(config.ConsoleJson, config.HumanReadableTimestamp), writer, logger.consoleLevel)
		cores = append(cores, core)
	}

	if config.EnableFile {
		writer := zapcore.AddSync(&lumberjack.Logger{
			Filename: config.FileLocation,
			MaxSize:  100,
			Compress: true,
		})
		core := zapcore.NewCore(makeEncoder(config.FileJson, config.HumanReadableTimestamp), writer, logger.fileLevel)
		cores = append(cores, core)
	}

	combinedCore := zapcore.NewTee(cores...)

	logger.zap = zap.New(combinedCore,
		zap.AddCaller(),
	)

	return logger
}

func (l *Logger) Debug(message string, fields ...Field) {
	l.zap.Debug(message, fields...)
}

func (l *Logger) Debugf(format string, v ...interface{}) {
	message := fmt.Sprintf(format, v...)
	l.zap.Debug(message)
}

func (l *Logger) Info(message string, fields ...Field) {
	l.zap.Info(message, fields...)
}

func (l *Logger) Infof(format string, v ...interface{}) {
	message := fmt.Sprintf(format, v...)
	l.zap.Info(message)
}

func (l *Logger) Warn(message string, fields ...Field) {
	l.zap.Warn(message, fields...)
}

func (l *Logger) Warnf(format string, v ...interface{}) {
	message := fmt.Sprintf(format, v...)
	l.zap.Warn(message)
}

func (l *Logger) Error(message string, fields ...Field) {
	l.zap.Error(message, fields...)
}

func (l *Logger) Errorf(format string, v ...interface{}) {
	message := fmt.Sprintf(format, v...)
	l.zap.Error(message)
}

func (l *Logger) Critical(message string, fields ...Field) {
	l.zap.Error(message, fields...)
}

func (l *Logger) Panic(message string, fields ...Field) {
	l.zap.Error(message, fields...)
	panic(fmt.Sprintln(message))
}
