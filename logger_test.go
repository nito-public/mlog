package mlog_test

import (
	"errors"
	"os"
	"runtime/debug"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/nito-public/mlog"
)

func TestLoggerPanic(t *testing.T) {
	logger := mlog.NewLogger(&mlog.Configuration{})

	require.NotPanics(t, func() {
		logger.Info("Info log")
		logger.Debug("Debug log")
		logger.Warn("Warn log")
	})

	require.Panics(t, func() {
		logger.Panic("panic log")
	})

	loggerPanic := mlog.NewLogger(&mlog.Configuration{
		EnableFile:   true,
		FileLocation: "./file.log",
		FileJson:     true,
	})
	defer os.Remove("./file.log")

	defer func() {
		if err := recover(); err != nil {
			loggerPanic.Critical("Application stopped by panic!!!", mlog.Any("error", err), mlog.StackTrace(string(debug.Stack())))
			actual := getLogText(t, "./file.log", true)
			require.Len(t, strings.Split(actual, "\n"), 1)
		}
	}()

	logger.Panic("panic log")
}

func TestLoggerFields(t *testing.T) {
	logger := mlog.NewLogger(&mlog.Configuration{
		EnableFile:   true,
		FileLocation: "./file.log",
		FileJson:     true,
	})

	err := errors.New("some random error")

	logger.Error("debug log",
		mlog.String("String", "string field"),
		mlog.Int("Int", 10),
		mlog.Uint32("Uint32", 4294967295),
		mlog.Int32("Int32", 2147483647),
		mlog.Int64("Int64", 9223372036854775807),
		mlog.Bool("Bool", true),
		mlog.Duration("Duration", 30*time.Second),
		mlog.Err(err),
		mlog.NamedErr("NamedError", err),
		mlog.Any("Any", []string{"one", "two"}),
	)
	defer os.Remove("./file.log")

	actual := getLogText(t, "./file.log", true)

	expected := `{"level":"error","ts":0,"caller":"mlog/logger.go:0","msg":"debug log","String":"string field","Int":10,"Uint32":4294967295,"Int32":2147483647,"Int64":9223372036854775807,"Bool":true,"Duration":30,"error":"some random error","NamedError":"some random error","Any":["one","two"]}`
	require.Equal(t, expected, actual)
}
