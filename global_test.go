package mlog_test

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/nito-public/mlog"
)

func getLogText(t *testing.T, filePath string, isJson bool) string {
	logs, err := ioutil.ReadFile(filePath)
	require.NoError(t, err)

	actual := strings.TrimSpace(string(logs))

	if isJson {
		reTs := regexp.MustCompile(`"ts":[0-9\.]+`)
		reCaller := regexp.MustCompile(`"caller":"([^"]+):[0-9\.]+"`)
		actual = reTs.ReplaceAllString(actual, `"ts":0`)
		actual = reCaller.ReplaceAllString(actual, `"caller":"$1:0"`)
	} else {
		actualRows := strings.Split(actual, "\n")
		for i, actualRow := range actualRows {
			actualFields := strings.Split(actualRow, "\t")
			if len(actualFields) > 3 {
				actualFields[0] = "TIME"
				reCaller := regexp.MustCompile(`([^"]+):[0-9\.]+`)
				actualFields[2] = reCaller.ReplaceAllString(actualFields[2], "$1:0")
				actualRows[i] = strings.Join(actualFields, "\t")
			}
		}

		actual = strings.Join(actualRows, "\n")
	}

	return actual
}

// https://github.com/zenizh/go-capturer/blob/master/main.go
func captureOutput(f func()) string {
	r, w, err := os.Pipe()
	if err != nil {
		panic(err)
	}

	stderr := os.Stderr
	os.Stderr = w
	defer func() {
		os.Stderr = stderr
	}()

	f()
	_ = w.Close()

	var buf bytes.Buffer
	_, _ = io.Copy(&buf, r)

	return buf.String()
}

func TestInvalidLog(t *testing.T) {
	output := captureOutput(func() {
		//Json marshal error
		mlog.Info("Invalid json", mlog.Any("foo", make(chan int)))
	})

	require.Equal(t, `{"level":"error","msg":"failed to encode log message"}`, strings.Split(output, "\n")[0])
}

func TestLoggingBeforeInitialized(t *testing.T) {
	require.NotPanics(t, func() {
		// None of these should segfault before mlog is globally configured
		mlog.Info("info log")
		mlog.Debug("debug log")
		mlog.Warn("warning log")
		mlog.Error("error log")
		mlog.Critical("critical log")

		mlog.Infof("info log %s", "info")
		mlog.Debugf("debug log %s", "debug")
		mlog.Warnf("warning log %v", "warning")
		mlog.Errorf("error log %v", "param")
	})
	require.Panics(t, func() {
		mlog.Panic("panic log")
	})
}

func TestLoggingAfterInitialized(t *testing.T) {
	testCases := []struct {
		Description         string
		LoggerConfiguration *mlog.Configuration
		ExpectedLogs        []string
	}{
		{
			"file logging, json, debug",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      true,
				FileLevel:     mlog.LevelDebug,
			},
			[]string{
				`{"level":"debug","ts":0,"caller":"mlog/global_test.go:0","msg":"real debug log"}`,
				`{"level":"info","ts":0,"caller":"mlog/global_test.go:0","msg":"real info log"}`,
				`{"level":"warn","ts":0,"caller":"mlog/global_test.go:0","msg":"real warning log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real error log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real critical log","error":"some random error"}`,
			},
		},
		{
			"file logging, json, info",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      true,
				FileLevel:     mlog.LevelInfo,
			},
			[]string{
				`{"level":"info","ts":0,"caller":"mlog/global_test.go:0","msg":"real info log"}`,
				`{"level":"warn","ts":0,"caller":"mlog/global_test.go:0","msg":"real warning log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real error log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real critical log","error":"some random error"}`,
			},
		},
		{
			"file logging, json, warn",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      true,
				FileLevel:     mlog.LevelWarn,
			},
			[]string{
				`{"level":"warn","ts":0,"caller":"mlog/global_test.go:0","msg":"real warning log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real error log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real critical log","error":"some random error"}`,
			},
		},
		{
			"file logging, json, error",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      true,
				FileLevel:     mlog.LevelError,
			},
			[]string{
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real error log"}`,
				`{"level":"error","ts":0,"caller":"mlog/global_test.go:0","msg":"real critical log","error":"some random error"}`,
			},
		},
		{
			"file logging, non-json, debug",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      false,
				FileLevel:     mlog.LevelDebug,
			},
			[]string{
				`TIME	debug	mlog/global_test.go:0	real debug log`,
				`TIME	info	mlog/global_test.go:0	real info log`,
				`TIME	warn	mlog/global_test.go:0	real warning log`,
				`TIME	error	mlog/global_test.go:0	real error log`,
				`TIME	error	mlog/global_test.go:0	real critical log	{"error": "some random error"}`,
			},
		},
		{
			"file logging, non-json, error",
			&mlog.Configuration{
				EnableConsole: false,
				EnableFile:    true,
				FileJson:      false,
				FileLevel:     mlog.LevelError,
			},
			[]string{
				`TIME	error	mlog/global_test.go:0	real error log`,
				`TIME	error	mlog/global_test.go:0	real critical log	{"error": "some random error"}`,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Description, func(t *testing.T) {
			var filePath string
			if testCase.LoggerConfiguration.EnableFile {
				tempDir, err := ioutil.TempDir(os.TempDir(), "TestLoggingAfterInitialized")
				require.NoError(t, err)
				defer os.Remove(tempDir)

				filePath = filepath.Join(tempDir, "file.log")
				testCase.LoggerConfiguration.FileLocation = filePath
			}

			logger := mlog.NewLogger(testCase.LoggerConfiguration)
			mlog.InitGlobalLogger(logger)

			err := errors.New("some random error")

			mlog.Debugf("real debug log")
			mlog.Infof("real info log")
			mlog.Warnf("real warning log")
			mlog.Errorf("real error log")
			mlog.Critical("real critical log", mlog.Err(err))

			if testCase.LoggerConfiguration.EnableFile {
				actual := getLogText(t, filePath, testCase.LoggerConfiguration.FileJson)
				require.ElementsMatch(t, testCase.ExpectedLogs, strings.Split(actual, "\n"))
			}
		})
	}
}

func TestRedirectStdLog(t *testing.T) {
	logger := mlog.NewLogger(&mlog.Configuration{
		EnableConsole: true,
		EnableFile:    true,
		FileJson:      true,
		FileLevel:     mlog.LevelDebug,
		FileLocation:  "./file.log",
	})
	mlog.InitGlobalLogger(logger)

	log.Println("Error without redirect")
	mlog.RedirectStdLog(logger)
	log.Println("Default log error")

	defer os.Remove("./file.log")

	actual := getLogText(t, "./file.log", true)
	require.Len(t, strings.Split(actual, "\n"), 1, "Should be one log")
	require.Equal(t, `{"level":"error","ts":0,"caller":"log/log.go:0","msg":"Default log error","source":"stdlog"}`, actual)
}
