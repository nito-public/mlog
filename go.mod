module gitlab.com/nito-public/mlog

go 1.15

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.19.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
