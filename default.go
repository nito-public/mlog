package mlog

import (
	"encoding/json"
	"fmt"
	"os"
)

// defaultLog manually encodes the log to STDERR, providing a basic, default logging implementation
// before logger is fully configured.
func defaultLog(level, msg string, fields ...Field) {
	log := struct {
		Level   string  `json:"level"`
		Message string  `json:"msg"`
		Fields  []Field `json:"fields,omitempty"`
	}{
		level,
		msg,
		fields,
	}

	if b, err := json.Marshal(log); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, `{"level":"error","msg":"failed to encode log message"}%s`, "\n")
	} else {
		_, _ = fmt.Fprintf(os.Stderr, "%s\n", b)
	}
}

func defaultDebugLog(msg string, fields ...Field) {
	defaultLog("debug", msg, fields...)
}

func defaultDebugLogf(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	defaultLog("debug", msg)
}

func defaultInfoLog(msg string, fields ...Field) {
	defaultLog("info", msg, fields...)
}

func defaultInfoLogf(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	defaultLog("info", msg)
}

func defaultWarnLog(msg string, fields ...Field) {
	defaultLog("warn", msg, fields...)
}

func defaultWarnLogf(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	defaultLog("warn", msg)
}

func defaultErrorLog(msg string, fields ...Field) {
	defaultLog("error", msg, fields...)
}

func defaultErrorLogf(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	defaultLog("error", msg)
}

func defaultCriticalLog(msg string, fields ...Field) {
	// We map critical to error in zap, so be consistent.
	defaultLog("error", msg, fields...)
}

func defaultPanicLog(msg string, fields ...Field) {
	// We map critical to error in zap, so be consistent.
	defaultLog("panic", msg, fields...)
	panic(msg)
}
