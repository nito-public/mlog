# Install
```shell
go get -u gitlab.com/nito-public/mlog
```

# Usage
## Basic usage
```go
import (
    "gitlab.com/nito-public/mlog"
)

...

logger := mlog.NewLogger(&mlog.Configuration{
    EnableConsole: true,
    ConsoleJson:   true,
    ConsoleLevel:  "INFO",
    EnableFile:    true,
    FileLocation:  "./logs/log.log",
    FileLevel:     "INFO",
})

logger.Info("Test log info")
```

## Using Global
```go
import (
    "gitlab.com/nito-public/mlog"
)

...

logger := mlog.NewLogger(&mlog.Configuration{
    EnableConsole: true,
    ConsoleJson:   true,
    ConsoleLevel:  "INFO",
    EnableFile:    true,
    FileLocation:  "./logs/log.log",
    FileLevel:     "INFO",
})

// Redirect default golang logger to this logger
mlog.RedirectStdLog(logger)

// Use this app logger as the global logger (eventually remove all instances of global logging)
mlog.InitGlobalLogger(logger)

// In other package
mlog.Info("Test log info" )
```